#include <iostream>
#include <cmath>
using namespace std;

double f(double x)
{
    return x*2;//    //
//  return sin(x*3.1415/180);
}
int n;
double x, h, s[2];

double T(double a, double b)
{
    s[0] =s[1] = 0;
    s[0] = f(a) + f(b);
    for(int i =1; i < n; i++)
        s[1]+= f(a+h*i);
    return (h/2)*(s[0]+2*s[1]);
}

double S13(double a, double b)
{
    s[0] =s[1] = s[2] = 0;
    s[0] = f(a) + f(b);
    for(int i=1; i < n; i++)
          if(i%2 != 0)
            s[1] += f(a+h*i);
        else
            s[2] += f(a+h*i);
    return (h/3)*(s[0]+4*s[1]+2*s[2]);
}

double S38(double a, double b)
{
    s[0] = s[1] = s[2] = 0;
    s[0] = f(a) + f(b);
    for (int i=1; i < n ; i++)
        if(i%3 == 0)
            s[1] += f(a+h*i);
        else
            s[2] += f(a+h*i);
    return (double)(3*h/8)*(s[0]+2*s[1]+3*s[2]);
}

int main()
{
    cout << "Enter the value of a, n and h:";
    cin >>x>>n>>h;

    cout << "Trapezoidal Sum = " << T(x, x+n*h)<< endl;
    cout << "Simpson's 1/3rd Sum = " << S13(x, x+n*h)<<endl;
    cout << "Simpson's Sum = " << S38(x, x+n*h)<<endl;

    return 0;
}
