#include <iostream>
#include <cmath>
using namespace std;

double f(double x)
{
    return x + x*x + x*x*x;//    //
//  return sin(x*3.1415/180);
}
int n;
double x, h,  s[2];

double T(double a, double b, int init, int fin)
{
    s[0] =s[1] = 0;
    s[0] = f(a) + f(b);
    for(int i =init+1; i < fin; i++)
        s[1]+= f(a+h*i);
    return (h/2)*(s[0]+2*s[1]);
}

double S13(double a, double b, int init, int fin)
{
    s[0] =s[1] = s[2] = 0;
    s[0] = f(a) + f(b);
    for(int i=init+1; i < fin; i++)
          if(i%2 != 0)
            s[1] += f(a+h*i);
        else
            s[2] += f(a+h*i);    
	return (h/3)*(s[0]+4*s[1]+2*s[2]);
}

double S38(double a, double b, int init, int fin)
{
    s[0] = s[1] = s[2] = 0;
    s[0] = f(a) + f(b);
    for (int i=init+1; i < fin; i++)
        if(i%3 == 0)
            s[1] += f(a+h*i);
        else
            s[2] += f(a+h*i);
    return (double)(3*h/8)*(s[0]+2*s[1]+3*s[2]);
}

int main()
{
    cout << "Enter the value of a, n and h:";
    cin >>x>>n>>h;

    double sum =0;
    if(n == 1)
	sum = T(x, x+n*h, 0, n);
    else if (n == 2)
	sum = S13(x, x+n*h, 0, n);
    else if(n%3 == 0)
        sum = S38(x, x+n*h, 0, n);
    else
    {
        if(n%3 == 2)
            sum = S38(x, x+(n-2)*h, 0, n-2) + S13(x+(n-2)*h, x+n*h, 0, 2);
        else
            sum = S38(x, x+(n-1)*h, 0, n-1) + T(x+(n-1)*h, x+n*h, 0, 1);
    }

    cout << "Combining all the methods we get sum = "<<sum;
    return 0;
}